# Ligo Neditor Backend

## Introduction

This is the backend code for the online (net) editor hosted at https://ligolang.org/.
It's written in [Flask](https://flask.palletsprojects.com/) and currently
deployed with [gunicorn](https://gunicorn.org/).

## Dependencies

You will need the python virtual environment creator installed to use the make
file. It can be installed with the command:

    sudo apt-get install virtualenv

## API

Right now the API only has one endpoint, at /dry-run. It takes essentially the
same parameters as the LIGO binary does for a dry run. A program to execute,
an entrypoint, and expressions defining the parameters to the contract as well
as previous stored state.

Programs are sent to the server for a dry run in the following JSON format:

```
{
    "program":"function main (const parameter : int;  const contractStorage : int) : (list(operation) * int) is \n block {skip} with ((nil : list(operation)), contractStorage + parameter)",
    "entrypoint":"main",
    "param_expr":"3",
    "stor_expr":"4"
}
```

This JSON is POSTed to the /dry-run endpoint, and a dry run is performed. You
might do it like this:

    wget --header="Content-Type: application/json" --post-file=taco_contract_post.json 127.0.0.1:<YOUR PORT>/api/0.1/dry-run

In a web application context, obviously you would use something other than wget
to send a POST request. If successful you should get back a JSON form with the
stdout of the LIGO binary.
