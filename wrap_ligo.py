from flask import Flask, escape, request, jsonify
from werkzeug.exceptions import HTTPException
import random
import subprocess
import json
import os

app = Flask(__name__)

# Utilities

class UploadTooBigError(HTTPException):
    code = 400
    # TODO: Figure out how to get Flask to let me specify the exact problem with
    # size.
    description = "One of the parameters in your request is too big, see " \
                  "documentation for size limits on requests."
    

def gen_random_string(length: int) -> str:
    random_string = ""
    pool = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
    for i in range(length):
        random_string += random.choice(pool)
    return random_string

def write_program_file(program: str) -> str:
    """Write the temporary file used to store a submitted LIGO program before 
    compilation."""
    temp_file_name = gen_random_string(25) + ".ligo"
    with open(temp_file_name, "w+") as tempfile:
        tempfile.write(program)
        tempfile.flush()
    return temp_file_name

def param_size_check(parameters: dict) -> None:
    """Check that the size of parameters is acceptable."""
    kilobyte = 1024
    megabyte = kilobyte * 1024
    size_limits = {
        "program":megabyte,
        "entrypoint":kilobyte,
        "syntax":kilobyte,
        "param_expr":kilobyte * 56,
        "stor_expr":kilobyte * 56,
    }
    for parameter in size_limits:
        if parameter in parameters:
            try:
                assert len(parameters[parameter]) <= size_limits[parameter]
            except AssertionError as e:
                raise UploadTooBigError(e)

# Compile Job Setup and Runners
            
def run_job(job_form: dict, runner):
    """Run a job for a LIGO subcommand."""
    temp_file_name = write_program_file(job_form["program"])
    # runner is a function that does a LIGO compiler run with some subcommand
    job_results = runner(temp_file_name, job_form)
    os.remove(temp_file_name)
    # Fixes weird thing where some syntaxes have .pp.ligo and some don't
    try:
        os.remove(temp_file_name.split(".")[0] + ".pp.ligo")
    except FileNotFoundError:
        pass
    return {"out":job_results}
    
def ligo_dry_run(temp_file_name, parameters) -> str:
    """Perform a LIGO dry run, simulating the execution of a contract."""
    try:
        syntax = parameters["syntax"]
    except KeyError:
        syntax = "pascaligo"
    entrypoint = parameters["entrypoint"]
    param_expr = parameters["param_expr"]
    stor_expr = parameters["stor_expr"]
    ligo_job = subprocess.Popen(
        ["ligo", "dry-run",
         "-s", syntax,
         temp_file_name, entrypoint,
         param_expr, stor_expr],
        stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    ligo_job.wait(timeout=30)
    # TODO: Expand this to include error parsing/etc
    results = ligo_job.communicate()
    return results[0].decode("UTF-8")

def ligo_compile_contract(temp_file_name, parameters) -> str:
    """Compile a LIGO program, currently to a text Michelson contract."""
    entrypoint = parameters["entrypoint"]
    try:
        syntax = parameters["syntax"]
    except KeyError:
        syntax = "pascaligo"
    ligo_job = subprocess.Popen(
        ["ligo", "compile-contract",
         "-s", syntax, temp_file_name, entrypoint],
        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    ligo_job.wait(timeout=30)
    results = ligo_job.communicate()
    return results[0].decode("UTF-8")

# API Endpoints and Routing

@app.route('/api/0.1/dry-run', methods=["GET","POST"])
def dry_run():
    if request.is_json:
        job_form = request.get_json()
        # Check that we haven't gotten a ludicrously large file
        param_size_check(job_form)
        job_results = run_job(job_form, ligo_dry_run)
    else:
        job_results = {"out":"You need to POST a script for me to run it!"}
    return jsonify(job_results)

@app.route('/api/0.1/compile-contract', methods=["GET","POST"])
def compile_contract():
    if request.is_json:
        job_form = request.get_json()
        param_size_check(job_form)
        job_results = run_job(job_form, ligo_compile_contract)
    else:
        job_results = {"out":"You need to POST a script for me to run it!"}
    return jsonify(job_results)
            
@app.errorhandler(UploadTooBigError)
def handle_upload_too_big(e):
    return "One of the parameters in your request is too big, see " \
        "documentation for size limits on requests.", 400

if __name__ == '__main__':
    app.run()
